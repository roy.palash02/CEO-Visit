//
//  ExecutivesTableViewCell.swift
//  ceo app
//
//  Created by Sravanthi Kuppireddy on 8/19/19.
//  Copyright © 2019 Palash Roy. All rights reserved.
//

import UIKit

class ExecutivesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageViewExecutive: UIImageView!
    
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var labelSubtitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.cornerRadius = 8.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.5
        self.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
