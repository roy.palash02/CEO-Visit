//
//  CustomUILabel.swift
//  ceo app
//
//  Created by Palash Roy on 8/16/19.
//  Copyright © 2019 Palash Roy. All rights reserved.
//

import UIKit

@IBDesignable
class CustomUILabel: UILabel {
    
    @IBInspectable var label_Rotation: Double = 0 {
        didSet {
            rotateLabel(labelRotation: label_Rotation)
            self.layoutIfNeeded()
        }
    }
    
    func rotateLabel(labelRotation: Double)  {
        self.transform = CGAffineTransform(rotationAngle: CGFloat((Double.pi * 2) + labelRotation))
    }
}
