//
//  ExecutivesViewController.swift
//  ceo app
//
//  Created by Sravanthi Kuppireddy on 8/18/19.
//  Copyright © 2019 Palash Roy. All rights reserved.
//

import UIKit

class ExecutivesViewController: SideBaseViewController {
    @IBOutlet weak var tableView_Executive_List: UITableView!
    
    @IBOutlet weak var searchBarView: UIView!
    
    @IBOutlet weak var searchBarTextField: UITextField!
    
    var isFiltered = false
    var array_ListOfExecutives = [["name": "K Krithivasan","desc":"President, Banking and Financial Support","image":"profile"],["name": "Bharathi Rathinavel","desc":"President, Banking and Financial Support","image":"profile"],["name": "Rijith","desc":"President, Banking and Financial Support","image":"profile"]]
    var filteredAry_ListOfExecutives = [[String:String]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Executive Profiles"
        self.ConfigureUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Get CustomTabBarViewController(which subclass of RAMAnimatedTabBarController) and call hide tabbar item method of RAMAnimatedTabBarController to hide tabitems
        (self.navigationController?.parent as? CustomTabBarViewController)?.animationTabBarHidden(false)
    }
    
    func ConfigureUI(){
        searchBarTextField.textColor = UIColor.white
        searchBarTextField.attributedPlaceholder = NSAttributedString(string: "Search", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        searchBarTextField.delegate = self
        
        guard let imageSize = UIImage.init(named:"search")?.size
            else { return }
        self.searchBarTextField.addPlaceHolderImage(direction: .Left, imageName: "search", Frame: CGRect(x: 10, y: 0, width: imageSize.width, height: imageSize.height), backgroundColor: .clear)
       
        searchBarTextField.addTarget(self, action: #selector(searchExecutives), for: .editingChanged)
        let cell = UINib (nibName: "ExecutivesTableViewCell", bundle: nil)
        tableView_Executive_List.register(cell, forCellReuseIdentifier: "ExecutivesTableViewCell")
        tableView_Executive_List.tableFooterView = UIView.init()
        
    }
    @objc func searchExecutives(_ textfield:UITextField){
        
        if textfield.text?.count != 0{
            isFiltered = true
            let searchText = textfield.text ?? ""
            filteredAry_ListOfExecutives = (array_ListOfExecutives.filter({( executive : [String:String]) -> Bool in
                
                return  executive["name"]?.lowercased().contains(searchText.lowercased()) ?? false
                
            }))
        }
        else {
            isFiltered = false
        }
        tableView_Executive_List.reloadData()
    }
    
}

extension ExecutivesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
         if isFiltered {
            return filteredAry_ListOfExecutives.count
         }else {
            return array_ListOfExecutives.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView_Executive_List.dequeueReusableCell(withIdentifier: "ExecutivesTableViewCell", for: indexPath) as! ExecutivesTableViewCell
        
        if isFiltered {
            cell.imageViewExecutive.image = UIImage.init(named: filteredAry_ListOfExecutives[indexPath.section]["image"]!)
            cell.labelTitle.text = filteredAry_ListOfExecutives[indexPath.section]["name"]
            cell.labelSubtitle.text = filteredAry_ListOfExecutives[indexPath.section]["desc"]
        }else {
            cell.imageViewExecutive.image = UIImage.init(named: array_ListOfExecutives[indexPath.section]["image"]!)
            cell.labelTitle.text = array_ListOfExecutives[indexPath.section]["name"]
            cell.labelSubtitle.text = array_ListOfExecutives[indexPath.section]["desc"]
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 10
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewObj = UIView(frame: CGRect(x:0,y:0,width:tableView.frame.width,height:10))
        return viewObj
    }
    
    
}
extension ExecutivesViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        searchBarTextField.resignFirstResponder()
        return true
    }
}
extension UITextField {
    
    enum Direction
    {
        case Left
        case Right
    }
    
    func addPlaceHolderImage(direction:Direction,imageName:String,Frame:CGRect,backgroundColor:UIColor)
    {
        let View = UIView(frame: Frame)
        View.backgroundColor = backgroundColor
        View.frame.size.width = View.frame.width + 17
        
        let imageView = UIImageView(frame: Frame)
        imageView.image = UIImage(named: imageName)
        
        View.addSubview(imageView)
        
        if Direction.Left == direction
        {
            self.leftViewMode = .always
            self.leftView = View
        }
        else
        {
            self.rightViewMode = .always
            self.rightView = View
        }
        
    }
    
    
    
    
}
